// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use std::collections::HashMap;

use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Settings {
    #[serde(default = "HashMap::new")]
    pub variables: HashMap<String, String>,
    pub commands: Vec<Vec<String>>,
    pub directories: Vec<Directory>,
}

#[derive(Deserialize, Debug)]
pub struct Directory {
    pub name: String,
    #[serde(default = "HashMap::new")]
    pub variables: HashMap<String, String>,
}
