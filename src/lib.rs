// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use std::collections::HashMap;
use std::ffi::OsString;
use std::fs::File;
use std::path::PathBuf;
use std::process::Command;

use anyhow::{anyhow, Result};
use clap::Clap;

pub mod options;
pub mod settings;

use options::Options;
use settings::Settings;

pub fn main<I, T>(arguments: I) -> Result<()>
where
    I: IntoIterator<Item = T>,
    T: Into<OsString> + Clone,
{
    let options = Options::parse_from(arguments);

    let settings: Settings = serde_yaml::from_reader(File::open(options.config)?)?;

    for cmd in &settings.commands {
        if cmd.is_empty() {
            return Err(anyhow!("commands cannot be empty!"));
        }
    }

    let directories = prepare_directories(&settings)?;

    execute_directories(&settings, directories)?;

    Ok(())
}

fn prepare_directories(settings: &Settings) -> Result<HashMap<PathBuf, &HashMap<String, String>>> {
    let mut directories = HashMap::new();
    for directory in &settings.directories {
        let dir = PathBuf::from(&directory.name);
        if dir.is_dir() {
            directories.insert(dir.canonicalize()?, &directory.variables);
        } else {
            return Err(anyhow!("directory '{}' does not exist!", directory.name));
        }
    }

    Ok(directories)
}

fn execute_directories(
    settings: &Settings,
    directories: HashMap<PathBuf, &HashMap<String, String>>,
) -> Result<()> {
    let cwd = std::env::current_dir()?;

    for (dir, variables) in directories {
        std::env::set_current_dir(&dir)?;
        println!("Executing commands in `{}`:", dir.to_string_lossy());

        let variables = get_specific_variables(settings, variables);

        for cmd in &settings.commands {
            println!("----------------------Executing {:?}----------------------", cmd);
            let mut command = get_command(cmd, &variables);
            if !command.status()?.success() {
                return Err(anyhow!("Command `{:?}` failed to execute!", cmd)); 
            }
        }

        println!("======================Finished execution======================");
    }

    std::env::set_current_dir(cwd)?;

    Ok(())
}

fn get_command(command: &[String], variables: &HashMap<String, String>) -> Command {
    let mut args = command.iter();
    let mut command = Command::new(apply_vars(args.next().unwrap(), variables));
    args.for_each(|arg| {
        command.arg(apply_vars(arg, variables));
    });

    command
}

fn apply_vars(arg: &str, variables: &HashMap<String, String>) -> String {
    let mut final_arg = arg.to_string();
    for (name, value) in variables {
        final_arg = final_arg.replace(format!("%{}%", name).as_str(), value);
    }
    final_arg
}

fn get_specific_variables(
    settings: &Settings,
    local_vars: &HashMap<String, String>,
) -> HashMap<String, String> {
    let mut vars = settings.variables.clone();

    for (name, val) in local_vars {
        vars.insert(name.clone(), val.clone());
    }

    vars
}
