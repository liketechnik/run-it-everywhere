// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

use std::path::PathBuf;

use clap::Clap;

#[derive(Clap, Debug)]
#[clap(
    name = "run-it-everywhere",
    author,
    version,
    about,
    after_help = r"Copyright (C) 2020

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/."
)]
pub struct Options {
    /// The configuration file.
    #[clap(short, long, default_value = "./.runit.yaml", validator=validate_file_exists)]
    pub config: String,
}

fn validate_file_exists(v: &str) -> Result<(), String> {
    if PathBuf::from(v).is_file() {
        Ok(())
    } else {
        Err(String::from("config file does not exist!"))
    }
}
