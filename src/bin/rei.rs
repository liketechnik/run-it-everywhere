// SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>
//
// SPDX-License-Identifier: MPL-2.0

fn main() -> anyhow::Result<()> {
    run_it_everywhere::main(std::env::args_os())
}
