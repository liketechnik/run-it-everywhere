<!--
SPDX-FileCopyrightText: 2020 Florian Warzecha <liketechnik@disroot.org>

SPDX-License-Identifier: MPL-2.0
-->

# run-it-everywhere

Run a set of commands in a set of directories.

## Index

* [About](#about)
* [Configuration](#configuration)
* [Current state](#current-state)
* [License](#license)

## About

Auto-generate previews of a multiple latex documents in the same
repository.

## Configuration

See the `.runit.yaml` file in this repository for an example.

## Current state

First working draft, e. g. don't use this if anything going wrong
would have serious consequences.

## License

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at http://mozilla.org/MPL/2.0/.
